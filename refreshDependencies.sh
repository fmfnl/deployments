#!/bin/bash

set -e

### This is a script to use to refresh the lockfiles for all charts in this repository
### Due to the setup of helmfile, this needs to be done for each environment separately,
### so this is a convenience script

helmfile -e staging deps
helmfile -e production deps
